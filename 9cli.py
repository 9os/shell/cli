import sys, os, datetime, fnmatch, stat as stat_mod, time

try:
    import colorama
    colorama.init()

    term_colors = {
        '800' : colorama.Fore.LIGHTRED_EX,
        '080' : colorama.Fore.LIGHTGREEN_EX,
        '008' : colorama.Fore.LIGHTBLUE_EX,
        '444' : colorama.Fore.LIGHTBLACK_EX,
          '4' : colorama.Fore.LIGHTBLACK_EX,
          '8' : colorama.Fore.LIGHTWHITE_EX,
          '6' : colorama.Fore.WHITE,
        '000' : colorama.Fore.BLACK,
       '-000' : colorama.Back.BLACK,
        ''    : colorama.Fore.RESET,
    }

    def term_color(color = ''):
        print(term_colors[color], end = '')

except ImportError:
    # >[https://github.com/11l-lang/11l/blob/master/11l.py]:‘sys.exit("Module eldf is not installed!\nPlease install it using this command:\n" + (sys.platform == 'win32')*(os.path.dirname(sys.executable) + '\\Scripts\\') + 'pip3 install eldf')’
    # < -'‘eldf’'+'‘colorama’'
    sys.exit("Module colorama is not installed!\nPlease install it using this command:\n" + (sys.platform == 'win32')*(os.path.dirname(sys.executable) + '\\Scripts\\') + 'pip3 install colorama')

print('0.10.2', end = '   ') # version number
#term_color('-000') # commented to improve appearance in Ubuntu terminal
term_color('800'); print('800', end = ' '); term_color('')
term_color('080'); print('080', end = ' '); term_color('')
term_color('008'); print('008', end = ' '); term_color('')
term_color('444'); print('444', end = ' '); term_color('')
term_color(  '4'); print(  '4', end = ' '); term_color('')
term_color(  '6'); print(  '6', end = ' '); term_color('')
term_color(  '8'); print(  '8', end = ' '); term_color('')
term_color('000'); print('000');            term_color('')

if not os.path.isdir('.-'):
    os.mkdir('.-')

undo_stack = []
redo_stack = []
if os.path.isfile('.-/.!'):
    undo_redo = open('.-/.!').read()
    if undo_redo.startswith("!!\n"):
        redo_stack = undo_redo[3:].split("\n")[::-1]
    else:
        undo_redo_arr = undo_redo.split("\n!!\n")
        if undo_redo_arr[0] != '': undo_stack = undo_redo_arr[0].split("\n")
        if len(undo_redo_arr) > 1: redo_stack = undo_redo_arr[1].split("\n")[::-1]

def write_undo_redo_file():
    f = open('.-/.!', 'w')
    if len(undo_stack):
        f.write("\n".join(undo_stack))
        if len(redo_stack):
            f.write("\n!!\n" + "\n".join(reversed(redo_stack)))
    elif len(redo_stack):
        f.write("!!\n" + "\n".join(reversed(redo_stack)))

def action(act):
    undo_stack.append(act)
    redo_stack.clear()
    write_undo_redo_file()

orig_cwd = os.getcwd()
prev_cwd = ''
cwd_changed = False

def os_chdir(newcwd, undo_or_redo):
    global prev_cwd, cwd_changed
    prev_cwd = os.getcwd()
    os.chdir(newcwd)
    if undo_or_redo:
        cwd_changed = True

def green(s): return term_colors['080'] + s + term_colors['4']
def red  (s): return term_colors['800'] + s + term_colors['4']
def lgray(s): return term_colors[  '6'] + s + term_colors['4']

def show(s):
    term_color('4')
    global cwd_changed
    if not cwd_changed:
        print(' ' * len(os.getcwd()) + ' ' + s)
    else:
        print(lgray(os.getcwd() + '>') + s)
        cwd_changed = False

def show_after_chdir(s):
    term_color('4')
    print(' ' * len(prev_cwd) + ' ' + s)

def os_rename(src, dst):
    if os.path.exists(dst):
        raise OSError() # On Unix, if dst exists and is a file, it will be replaced silently if the user has permission.
    os.rename(src, dst)

def copy_file(src_name, dst_name, nfiles = None):
    dst = open(dst_name, 'xb')
    src = open(src_name, 'rb')
    BLOCK_SIZE = 64*1024
    global global_total_files, total_bytes_copied, total_files_size, progress_prev_print_time

    while True:
        buf = src.read(BLOCK_SIZE)
        if len(buf) == 0:
            break
        dst.write(buf)
        total_bytes_copied += len(buf)
        if time.perf_counter() - progress_prev_print_time >= 1.0:
            print("\r", end = '')
            if nfiles is not None:
                print(f'[{nfiles:{len(str(global_total_files))}} / {global_total_files} ({nfiles * 100 // global_total_files}%)] ', end = '')
            if total_bytes_copied < 1_000_000 and total_files_size >= 1_000_000:
                term_color('4')
            print_file_size(total_bytes_copied, len(f'{total_files_size:_}'), True)
            if total_bytes_copied < 1_000_000 and total_files_size >= 1_000_000:
                term_color('6')
            print(' / ', end = '')
            print_file_size(total_files_size, 0, True)
            print(f' ({total_bytes_copied * 100 // total_files_size}%)', end = '', flush = True)
            progress_prev_print_time = time.perf_counter()

    dst.flush()

    if sys.platform == 'win32':
        import ctypes, msvcrt
        creation_time = ctypes.wintypes.FILETIME()
        change_time = ctypes.wintypes.FILETIME()
        copy_time = ctypes.wintypes.FILETIME()
        ctypes.windll.kernel32.GetSystemTimePreciseAsFileTime(ctypes.byref(copy_time))
        # [https://stackoverflow.com/questions/6135805/how-to-get-a-win32-handle-of-an-open-file-in-python <- google:‘python get windows file handle’]
        if ctypes.windll.kernel32.GetFileTime(msvcrt.get_osfhandle(src.fileno()), ctypes.byref(creation_time), None, ctypes.byref(change_time)) == 0:
            raise ctypes.WinError()
        if ctypes.windll.kernel32.SetFileTime(msvcrt.get_osfhandle(dst.fileno()), ctypes.byref(creation_time), ctypes.byref(copy_time), ctypes.byref(change_time)) == 0:
            raise ctypes.WinError()
    else:
        os.utime(dst.fileno(), ns = (time.time_ns() if sys.version_info >= (3, 7) else
                                 int(time.time() * 1e9), # [https://stackoverflow.com/questions/55774054/precise-time-in-nano-seconds-for-python-3-6-and-earlier <- google:‘time_ns python 3.6’]
                                     os.stat(src.fileno()).st_mtime_ns))

    src.close()
    dst.close()

def gather_files_list(files_list, basedir, dirname = ''):
    global global_total_files, total_files_size, progress_prev_print_time
    files_list.append(dirname + '/')
    for de in os.scandir(basedir + dirname):
        if not de.is_symlink():
            if de.is_file():
                files_list.append(dirname + '/' + de.name)
                total_files_size += de.stat().st_size
                global_total_files += 1
                if global_total_files % 1000 == 0 and time.perf_counter() - progress_prev_print_time >= 1.0:
                    print(f"\r[0 / {global_total_files}]", end = '')
                    progress_prev_print_time = time.perf_counter()
            elif de.is_dir():
                gather_files_list(files_list, basedir, dirname + '/' + de.name)
    files_list.append(dirname + '\\')

def uncopy_file(src_name, dst_name, src = None):
    if src is None:
        src = os.stat(src_name)
    dst = os.stat(dst_name)
    if src.st_size == dst.st_size and src.st_mtime_ns == dst.st_mtime_ns and (
            sys.platform != 'win32' or # there is no way to set the creation/birth time of the file on Unix
            creation_time_ns_from_stat(src) ==
            creation_time_ns_from_stat(dst)):
        os.remove(dst_name)
        return True
    return False

class UncopyFileError(Exception):
    pass

def uncopy_dir(src, dst, dirname = ''):
    global global_total_files, progress_prev_print_time
    for de in os.scandir(src + dirname):
        if not de.is_symlink():
            if de.is_file():
                try:
                    if not uncopy_file(src + dirname + de.name, dst + dirname + de.name, de.stat()):
                        raise Exception()
                except:
                    print("\r", end = '') # clear progress
                    show(src + dirname + de.name + red(' <= ') + dst + dirname + de.name)
                    raise UncopyFileError()
                global_total_files += 1
                if global_total_files % 100 == 0 and time.perf_counter() - progress_prev_print_time >= 1.0:
                    print(f"\r[{global_total_files}]", end = '')
                    progress_prev_print_time = time.perf_counter()
            elif de.is_dir():
                uncopy_dir(src, dst, dirname + de.name + '/')
    try: os.rmdir(dst + dirname)
    except OSError: pass

def rstrip1(s, tup):
    for tu in tup:
        if s.endswith(tu): return s[:-len(tu)]
    return s

COMMAND_OK = 0
COMMAND_ERROR = 1
COMMAND_UNKNOWN = 2

def do_command(command, redo = False):
    global total_bytes_copied, total_files_size, progress_prev_print_time

    if '->' in command:
        src, dst = command.split('->', 1)

        try:
            if dst.rstrip(' ').endswith('/'):
                os_rename(src.strip(' '), dst.strip(' ') + os.path.basename(src.strip(' ')))
            else:
                if os.path.isdir(dst.strip(' ')):
                    show(src + '->' + red(dst.rstrip(' ')) + green('/'))
                    return COMMAND_ERROR, ''

                os_rename(src.strip(' '), dst.strip(' '))

        except FileNotFoundError:
            if not os.path.exists(src.strip(' ')):
                show(red(src) + '->' + dst)
            elif not os.path.isdir(os.path.dirname(dst.strip(' '))):
                show(src + '->' + red(dst))
            else:
                show(src + red('->') + dst)
            return COMMAND_ERROR, ''

        except OSError:
            show(src + red('->') + ''.join(red(c) if c in r'\/:*?"<>|' else c for c in dst))
            return COMMAND_ERROR, ''

        show(src + green('->') + dst)

        return COMMAND_OK, command

    elif '~>' in command:
        src, dst = command.split('~>', 1)
        src_name = src.strip(' ')
        dst_name = dst.strip(' ')

        if dst_name.endswith('/'):
            dst_name += os.path.basename(src_name.rstrip('/'))

        if not os.path.exists(src_name.rstrip('/')):
            show(red(src) + '~>' + dst)
            return COMMAND_ERROR, ''
        elif os.path.exists(dst_name):
            show(src + '~>' + red(dst))
            return COMMAND_ERROR, ''

        try:
            if src_name.endswith('/'):
                if not os.path.isdir(src_name[:-1]):
                    src_before_slash, src_slash = src.rsplit('/', 1)
                    show(src_before_slash + red('/' + src_slash) + '~>' + dst)
                    return COMMAND_ERROR, ''
                os.symlink(os.path.relpath(src_name[:-1], os.path.dirname(dst_name)), dst_name, True)
            else:
                if os.path.isdir(src_name):
                    src_r = src.rstrip(' ')
                    show(src_r + green('/') + src[len(src_r):] + red('~>') + dst)
                    return COMMAND_ERROR, ''
                os.symlink(os.path.relpath(src_name, os.path.dirname(dst_name)), dst_name)
        except OSError:
            show(src + red('~>') + dst)
            return COMMAND_ERROR, ''

        show(src + green('~>') + dst)

        return COMMAND_OK, command

    elif '=>' in command:
        src, dst = command.split('=>', 1)
        dst_name = dst.strip(' ')

        if src.rstrip(' ').endswith('/**'):
            if not dst_name.endswith('/'):
                show(src + '=>' + red(dst.rstrip(' ')) + green('/'))
                return COMMAND_ERROR, ''

            global global_total_files, total_files_size
            global_total_files = 0
            total_files_size = 0
            progress_prev_print_time = time.perf_counter()
            term_color('6') # for correct progress color
            files_list = []
            src_name = src.strip(' ')[:-2]
            gather_files_list(files_list, src_name)
            print(f"\r[0 / {global_total_files}]", end = '')

            total_bytes_copied = 0
            progress_prev_print_time = time.perf_counter()
            nfiles = 0
            try:
                for file_name in files_list:
                    if file_name.endswith('/'):
                        try: os.mkdir(dst_name + file_name)
                        except FileExistsError: pass
                    elif file_name.endswith('\\'):
                        if sys.platform == 'win32':
                            import ctypes
                            creation_time = ctypes.wintypes.FILETIME()
                            change_time = ctypes.wintypes.FILETIME()
                            copy_time = ctypes.wintypes.FILETIME()
                            ctypes.windll.kernel32.GetSystemTimePreciseAsFileTime(ctypes.byref(copy_time))

                            FILE_READ_ATTRIBUTES  = 0x80
                            FILE_WRITE_ATTRIBUTES = 0x100
                            OPEN_EXISTING = 3
                            FILE_FLAG_BACKUP_SEMANTICS = 0x02000000
                            hdir = ctypes.windll.kernel32.CreateFileW(src_name + file_name[:-1], FILE_READ_ATTRIBUTES, 0, None, OPEN_EXISTING, FILE_FLAG_BACKUP_SEMANTICS, None)
                            if hdir == -1: raise ctypes.WinError()
                            if ctypes.windll.kernel32.GetFileTime(hdir, ctypes.byref(creation_time), None, ctypes.byref(change_time)) == 0:
                                raise ctypes.WinError()
                            ctypes.windll.kernel32.CloseHandle(hdir)

                            hdir = ctypes.windll.kernel32.CreateFileW(dst_name + file_name[:-1], FILE_WRITE_ATTRIBUTES, 0, None, OPEN_EXISTING, FILE_FLAG_BACKUP_SEMANTICS, None)
                            if hdir == -1: raise ctypes.WinError()
                            if ctypes.windll.kernel32.SetFileTime(hdir, ctypes.byref(creation_time), ctypes.byref(copy_time), ctypes.byref(change_time)) == 0:
                                raise ctypes.WinError()
                            ctypes.windll.kernel32.CloseHandle(hdir)

                        else:
                            os.utime(dst_name + file_name[:-1], ns = (time.time_ns() if sys.version_info >= (3, 7) else
                                                                  int(time.time() * 1e9),
                                                                      os.stat(src_name + file_name[:-1]).st_mtime_ns))
                    else:
                        copy_file(src_name + file_name, dst_name + file_name, nfiles)
                        nfiles += 1
            except:
                show(src_name + file_name + red(' => ') + dst_name + file_name)
                return COMMAND_ERROR, ''

            print(f"\r[{nfiles} / {global_total_files} (100%)] ", end = '')
            print_file_size(total_files_size, 0, True); print(' / ', end = '')
            print_file_size(total_files_size, 0, True); print(' (100%)')
            show(src + green('=>') + dst)
            return COMMAND_OK, command

        # Copy single file
        if dst_name.endswith('/'):
            dst_name += os.path.basename(src.strip(' '))
        else:
            if os.path.isdir(dst.strip(' ')):
                show(src + '=>' + red(dst.rstrip(' ')) + green('/'))
                return COMMAND_ERROR, ''

        if not os.path.isfile(src.strip(' ')):
            show(red(src) + '=>' + dst)
            return COMMAND_ERROR, ''
        if os.path.exists(dst_name):
            show(src + '=>' + red(dst))
            return COMMAND_ERROR, ''

        term_color('6') # for correct progress color
        try:
            total_bytes_copied = 0
            total_files_size = os.path.getsize(src.strip(' '))
            progress_prev_print_time = time.perf_counter()
            copy_file(src.strip(' '), dst_name)
        except:
            show(src + red('=>') + dst)
            return COMMAND_ERROR, ''
        print("\r", end = '') # clear progress
        print_file_size(total_files_size, 0, True); print(' / ', end = '')
        print_file_size(total_files_size, 0, True); print(' (100%)')
        show(src + green('=>') + dst)
        return COMMAND_OK, command

    elif command.startswith('- '):
        name = command[2:].strip(' ')
        if name.endswith(('/', '/*/*', '/**')):
            name = rstrip1(name, ('/', '/*/*', '/**'))
            exists = os.path.isdir(name)
        else:
            exists = os.path.isfile(name)
        if exists:
            try:
                abspath, fname = os.path.split(os.path.abspath(name))
                _, abspath = os.path.splitdrive(abspath)
                os.makedirs('.-' + abspath, exist_ok = True)
                new_name = '.-' + abspath + '/' + fname
                pluses = ''
                while os.path.exists(new_name + pluses):
                    pluses += '+'
                os.rename(name, new_name + pluses)
            except:
                show(red('- ') + command[2:])
                return COMMAND_ERROR, ''

            show(green('- ') + command[2:])
            return COMMAND_OK, command + ' <|> '*(pluses != '') + pluses

        else:
            show('- ' + red(command[2:]))
            return COMMAND_ERROR, ''

    elif command.startswith('+ '):
        dirname = command[2:].strip(' ')
        if not dirname.endswith('/'):
            show(red('?'))
            return COMMAND_ERROR, ''

        try:
            os.mkdir(dirname)
        except:
            show(red('+ ') + command[2:])
            return COMMAND_ERROR, ''

        show(green('+ ') + command[2:])
        return COMMAND_OK, command

    elif command.startswith(('. =', '.=')):
        left = command[:2 if command.startswith('.=') else 3]
        right = command[len(left):]
        newcwd = right.strip(' ')

        if newcwd[1:2] == ':' and newcwd[2:3] not in ('\\', '/'): # forbid `. = C:` and `. = C:dir`
            coloni = right.index(':') + 1
            show(left + red(right[:coloni]) + right[coloni:])
            return COMMAND_ERROR, ''

        rpath = os.path.relpath('.', newcwd).replace('\\', '/')

        try:
            os.rename('.-', newcwd + '/.-')
            os_chdir(newcwd, redo)

        except FileNotFoundError:
            show(left + red(right))
            return COMMAND_ERROR, ''

        if redo:
            show_after_chdir(green(left) + right)

        return COMMAND_OK, command + ' <|> ' + rpath

    return COMMAND_UNKNOWN, command

def undo_commands(n):
    update_undo_redo_file = False

    for _ in range(n):
        if len(undo_stack) == 0:
            show(red('0'))
            break

        cmd = undo_stack[-1]

        if '->' in cmd:
            src, dst = cmd.split('->', 1)
            try:
                if dst.rstrip(' ').endswith('/'):
                    os_rename(dst.strip(' ') + os.path.basename(src.strip(' ')), src.strip(' '))
                else:
                    os_rename(dst.strip(' '), src.strip(' '))
            except:
                show(src + red('<-') + dst)
                break
            show(src + green('<-') + dst)

        elif '~>' in cmd:
            src, dst = cmd.split('~>', 1)
            src_name = src.strip(' ')
            dst_name = dst.strip(' ')

            if dst_name.endswith('/'):
                dst_name += os.path.basename(src_name.rstrip('/'))

            if not os.path.islink(dst_name):
                show(src + '<~' + red(dst))
                break

            try:
                if sys.platform == 'win32' and src_name.endswith('/'):
                    os.rmdir(dst_name)
                else:
                    os.unlink(dst_name)
            except:
                show(src + red('<~') + dst)
                break
            show(src + green('<~') + dst)

        elif '=>' in cmd:
            src, dst = cmd.split('=>', 1)

            if src.rstrip(' ').endswith('/**'):
                assert(dst.rstrip(' ').endswith('/'))
                global global_total_files, progress_prev_print_time
                global_total_files = 0
                progress_prev_print_time = time.perf_counter()
                term_color('6') # for correct progress color
                try:
                    uncopy_dir(src.strip(' ')[:-2], dst.strip(' '))
                except UncopyFileError:
                    break
                except:
                    print("\r", end = '') # clear progress
                    show(src + red('<=') + dst)
                    break
                print("\r", end = '') # clear progress
                show(src + green('<=') + dst)

            else:
                if not uncopy_file(src.strip(' '), dst.strip(' ') + (os.path.basename(src.strip(' ')) if dst.rstrip(' ').endswith('/') else '')):
                    show(src + red('<=') + dst)
                    break
                show(src + green('<=') + dst)

        elif cmd.startswith('- '):
            pluses = ''
            if ' <|> ' in cmd:
                cmd, pluses = cmd.split(' <|> ')
            name = rstrip1(cmd[2:].strip(' '), ('/', '/*/*', '/**'))
            try:
                abspath, fname = os.path.split(os.path.abspath(name))
                _, abspath = os.path.splitdrive(abspath)
                os_rename('.-' + abspath + '/' + fname + pluses, name)
                try: os.removedirs('.-' + abspath)
                except OSError: pass
            except:
                show(red('!- ') + cmd[2:])
                break
            show(green('!- ') + cmd[2:])

        elif cmd.startswith('+ '):
            dirname = cmd[2:].strip(' ')
            try:
                os.rmdir(dirname)
            except:
                show(red('- ') + cmd[2:])
                break
            show(green('- ') + cmd[2:])

        elif ' <|> ' in cmd:
            ocmd = cmd
            cmd, rcmd = cmd.split(' <|> ')

            if cmd.startswith(('. =', '.=')):
                n = 2 if cmd.startswith('.=') else 3
                try:
                    os.rename('.-', rcmd + '/.-')
                    os_chdir(rcmd, True)
                except:
                    show(red(cmd[:n]) + ocmd[n:])
                    break
                show_after_chdir(green(cmd[:n]) + ocmd[n:])

            else:
                show(red(cmd))
                break

        else:
            show(red(cmd))
            break

        redo_stack.append(cmd)
        undo_stack.pop()
        update_undo_redo_file = True

    global cwd_changed
    cwd_changed = False

    if update_undo_redo_file:
        write_undo_redo_file()

def redo_commands(n):
    update_undo_redo_file = False

    for _ in range(n):
        if len(redo_stack) == 0:
            show(red('0'))
            break

        cmd = redo_stack[-1]

        res, cmd = do_command(cmd, redo = True)

        if res == COMMAND_UNKNOWN:
            show(red(cmd))
            break
        elif res == COMMAND_ERROR:
            break

        undo_stack.append(cmd)
        redo_stack.pop()
        update_undo_redo_file = True

    global cwd_changed
    cwd_changed = False

    if update_undo_redo_file:
        write_undo_redo_file()

def show_commands(stack, n_str):
    if n_str == '':
        n = 1
    elif n_str.isdigit():
        n = int(n_str)
    elif n_str == '*':
        n = len(stack)
    else:
        show(red('?'))
        return

    for i in range(n):
        if i == len(stack):
            show(red('0'))
            break

        show(stack[-1-i])

def file_size_on_disk(stat, fname):
    if sys.platform == 'win32':
        # >[https://stackoverflow.com/questions/9903679/how-do-i-query-size-on-disk-file-information <- https://www.reddit.com/r/PowerShell/comments/cwf62w/retrieve_the_actual_size_occupied_by_a_file_on/ <- google:‘winapi get file "size on disk"’]:‘GetFileInformationByHandleEx’
        # >[https://stackoverflow.com/questions/4274899/get-actual-disk-space-of-a-file <- google:‘python "size on disk"’]:‘ctypes.windll.kernel32.GetCompressedFileSizeW’
        # >[https://www.iditect.com/programming/python-example/python-get-file-id-of-windows-file.html <- google:‘"ctypes.windll.kernel32.GetFileInformationByHandleEx"’]:‘GetFileInformationByHandleEx(hFile, 2, ctypes.byref(file_info), ctypes.sizeof(file_info))’
        import ctypes

        OPEN_EXISTING = 3
        hFile = ctypes.windll.kernel32.CreateFileW(fname, 0, 0, None, OPEN_EXISTING, 0, None)
        if hFile == -1:
            raise ctypes.WinError()

        class FILE_STANDARD_INFO(ctypes.Structure):
            _fields_ = [('AllocationSize', ctypes.wintypes.LARGE_INTEGER),
                        ('EndOfFile',      ctypes.wintypes.LARGE_INTEGER),
                        ('NumberOfLinks',  ctypes.wintypes.DWORD),
                        ('DeletePending',  ctypes.wintypes.BOOLEAN),
                        ('Directory',      ctypes.wintypes.BOOLEAN)]

        file_info = FILE_STANDARD_INFO()
        if ctypes.windll.kernel32.GetFileInformationByHandleEx(hFile, 1, ctypes.byref(file_info), ctypes.sizeof(file_info)) == 0:
            raise ctypes.WinError()

        ctypes.windll.kernel32.CloseHandle(hFile)
        return file_info.AllocationSize

    else:
        return stat.st_blocks * 512 # from [https://stackoverflow.com/questions/4274899/get-actual-disk-space-of-a-file <- google:‘python "size on disk"’]

def creation_time_ns_from_stat(s):
    if sys.platform == 'win32':
        return s.st_birthtime_ns if sys.version_info >= (3, 12) else s.st_ctime_ns
    try:
        return s.st_birthtime_ns # >[https://docs.python.org/3/library/os.html]:‘This attribute is not always available, and may raise AttributeError.’
    except AttributeError:
        return None

def format_time_s(ns):
    sec = ns // 1_000_000_000
    return datetime.datetime.fromtimestamp(sec).strftime('%Y-%m-%d %H:%M:%S')

def format_time(ns):
    nano = ns % 1_000_000_000
    return format_time_s(ns) + term_colors['4'] + f".{nano:011_}".replace('_', "'")

def print_times(stat):
    ctime_ns = creation_time_ns_from_stat(stat)
    term_color('6'); print('+ ' + (format_time(ctime_ns) if ctime_ns is not None else '?'))
    term_color('6'); print('* ' + term_colors['4']*(stat.st_mtime_ns == ctime_ns) + format_time(stat.st_mtime_ns) + '   * > ='*(stat.st_mtime_ns > stat.st_atime_ns and stat.st_atime_ns != ctime_ns))
    term_color('6'); print('= ' + term_colors['4']*(stat.st_atime_ns == ctime_ns) + format_time(stat.st_atime_ns))

def format_creation_time_s(stat):
    ctime_ns = creation_time_ns_from_stat(stat)
    return format_time_s(ctime_ns) if ctime_ns is not None else '?'

def print_file_size(file_size, l = 0, restore_lgray = False):
    if file_size >= 1_000_000:
        rem = file_size % 1_000_000
        print(f'{file_size // 1_000_000:{max(l-8, 0)}_}'.replace('_', "'") + term_colors['4'] + f"'{rem // 1000:03}'{rem % 1000:03}", end = '')
        if restore_lgray:
            term_color('6')
    else:
        print(f'{file_size:{l}_}'.replace('_', "'"), end = '')

def calc_dir_size(dirname):
    global global_total_files, progress_prev_print_time
    total_files = 0
    total_size = 0
    total_size_on_disk = 0
    for de in os.scandir(dirname):
        if not de.is_symlink():
            if de.is_file():
                total_files += 1
                total_size += de.stat().st_size
                #total_size_on_disk += file_size_on_disk(de.stat(), dirname + '/' + de.name)
                global_total_files += 1
                if global_total_files % 1000 == 0 and time.perf_counter() - progress_prev_print_time >= 1.0:
                    print(f"\r[{global_total_files}]", end = '')
                    progress_prev_print_time = time.perf_counter()
            elif de.is_dir():
                nfiles, files_size, files_size_on_disk = calc_dir_size(dirname + '/' + de.name)
                total_files += nfiles
                total_size += files_size
                total_size_on_disk += files_size_on_disk
    return total_files, total_size, total_size_on_disk

try:
  while True:
    term_color('4')
    print(os.getcwd().replace('\\', '/'), end = '>')
    term_color('8')
    command = input()

    res, command = do_command(command)
    if res == COMMAND_OK:
        action(command)
        if not command.startswith(('. =', '.=')):
            print()
        continue

    if res != COMMAND_UNKNOWN:
        assert(res == COMMAND_ERROR)
        print()
        continue

    if command == '!':
        undo_commands(1)
    elif command.startswith('!') and command[1:].isdigit():
        undo_commands(int(command[1:]))
    elif command == '!*':
        undo_commands(len(undo_stack))

    elif command == '!!':
        redo_commands(1)
    elif command.startswith('!!') and command[2:].isdigit():
        redo_commands(int(command[2:]))
    elif command == '!!*':
        redo_commands(len(redo_stack))

    elif command[:3] in ('!!?', '?!!'):
        show_commands(redo_stack, command[3:])
    elif command[:2] in ('!?', '?!'):
        show_commands(undo_stack, command[2:])

    elif command.startswith('?') and ' ' in command:
        cmd, oname = command.split(' ', maxsplit = 1)
        name = oname.strip(' ')

        if name.endswith(':'): # `? 0:` / `? *:`
            def print_disk_info(name): # >[google:‘python disk free space’]:‘Python | shutil.disk_usage() method’
                import shutil
                du = shutil.disk_usage(name)

                def print_giga(color, n):
                    if n >= 1_000_000_000:
                        rem = n % 1_000_000_000
                        n //= 1_000_000_000
                        term_color(color)
                        print(f'{n:_}'.replace('_', "'"), end = '')
                        term_color('4')
                        print(f"'{rem:011_}".replace('_', "'"), end = '')
                    else:
                        term_color('4')
                        print(f'{n:_}'.replace('_', "'"), end = '')

                print_giga('080', du.free)
                term_color('6')
                print(' / ', end = '')
                print_giga('6', du.total)
                print()

            if sys.platform == 'win32':
                if name == '*:':
                    for i in range(26):
                        name = chr(ord('A') + i) + ':\\'
                        if os.path.isdir(name):
                            term_color('6')
                            print(name[:-1], end = ' ')
                            print_disk_info(name)
                else:
                    if name[:-1].isdigit():
                        dn = int(name[:-1])
                        name = ['C', 'A', 'B'][dn] if dn < 3 else chr(ord('A') + dn)
                        name += ':'

                    if os.path.isdir(name + '\\'):
                        print_disk_info(name + '\\')
                    else:
                        show(cmd + ' ' + red(name[:-1]) + ':')
            else:
                if name in ('0:', '/:', '*:'):
                    print_disk_info('/')
                else:
                    show(cmd + ' ' + red(name[:-1]) + ':')

        elif '*' in name: # `? *` / `? subdir/*` / `?+* *` / `?+*/\+ *` / `?+*/\+5 *.txt`
            reverse = False
            sort_fn = lambda s_name: s_name[1].casefold()
            limit = 1000 # limitation for large directories
            display_fns = []

            calc_dir_sizes = False
            i = 1
            if i < len(cmd) and cmd[i] == '/': # `?/\/10 downloads/*`
                if cmd[i+1:i+2] == '\\' and cmd[i+2:i+3] != '/':
                    pass # `?/\10 downloads/*`
                else:
                    calc_dir_sizes = True
                    i += 1

            while i < len(cmd):
                if cmd[i:i+2] in ('/\\', '\\/'):
                    if cmd[i:i+2] == '\\/':
                        reverse = True
                    i += 2
                    if i < len(cmd) and not cmd[i].isdigit():
                        if   cmd[i] == '+': sort_fn = lambda s_name: creation_time_ns_from_stat(s_name[0]) or 0
                        elif cmd[i] == '*': sort_fn = lambda s_name: s_name[0].st_mtime_ns
                        elif cmd[i] == '=': sort_fn = lambda s_name: s_name[0].st_atime_ns
                        else:
                            show(cmd[:i] + red(cmd[i]) + cmd[i+1:] + ' ' + oname)
                            break
                        i += 1
                    else:
                        sort_fn = lambda s_name_size: s_name_size[2]
                    if i < len(cmd):
                        if cmd[i].isdigit():
                            if not cmd[i:].isdigit():
                                i += 1
                                while cmd[i].isdigit():
                                    i += 1
                                show(cmd[:i] + red(cmd[i:]) + ' ' + oname)
                                break
                            limit = int(cmd[i:])
                            i = len(cmd)
                        else:
                            show(cmd[:i] + red(cmd[i]) + cmd[i+1:] + ' ' + oname)
                            break
                else:
                    if   cmd[i] == '+': display_fns.append(lambda s: format_creation_time_s(s))
                    elif cmd[i] == '*': display_fns.append(lambda s: format_time_s(s.st_mtime_ns))
                    elif cmd[i] == '=': display_fns.append(lambda s: format_time_s(s.st_atime_ns))
                    else:
                        show(cmd[:i] + red(cmd[i]) + cmd[i+1:] + ' ' + oname)
                        break
                    i += 1
            else:
                only_dirs = False
                if name.endswith('/*/') or name == '*/': # `?/\/10 downloads/*/`
                    only_dirs = True
                    name = name.rstrip('/')

                dirname = os.path.dirname(name) or '.'
                if not os.path.isdir(dirname) and name != '.*/':
                    show(cmd + ' ' + red(oname))

                else:
                    if calc_dir_sizes:
                        global_total_files = 0
                        progress_prev_print_time = time.perf_counter()
                    term_color('6') # for correct progress color
                    def get_stat(de, name):
                        s = de.stat()
                        size = s.st_size
                        if de.is_dir():
                            if calc_dir_sizes:
                                size = calc_dir_size(name)[1]
                            else:
                                size = 0
                        return s, de.name, size
                    stats = []
                    if name == '.*/':
                        for de in os.scandir('.'):
                            if de.name[0] == '.' and de.is_dir():
                                stats.append(get_stat(de, de.name))
                    else:
                        pattern = os.path.basename(name)
                        for de in os.scandir(dirname):
                            if de.name[0] == '.' and de.is_dir(): # skip hidden directories
                                continue
                            if not de.is_dir() and only_dirs:
                                continue
                            if fnmatch.fnmatch(de.name, pattern):
                                stats.append(get_stat(de, dirname + '/' + de.name))
                    stats.sort(key = sort_fn, reverse = reverse)

                    max_file_size = max((size for s, name, size in stats), default = 0)
                    file_size_column_len = len(f'{max_file_size:_}')

                    print("\r", end = '') # clear progress
                    for s, name, size in stats[:limit]:
                        for display_fn in display_fns:
                            print(display_fn(s), end = '  ')
                        if stat_mod.S_ISDIR(s.st_mode) and not calc_dir_sizes:
                            print(' ' * file_size_column_len, end = '')
                        else:
                            print_file_size(size, file_size_column_len, True)
                        print('  ' + name + (term_colors['4'] + '/' + term_colors['6'] if stat_mod.S_ISDIR(s.st_mode) else ''))

        elif name.endswith('/'): # `? subdir/`
            if command[1] == '/': # `?/ subdir/`
                term_color('6')
                #global global_total_files, progress_prev_print_time
                global_total_files = 0
                progress_prev_print_time = time.perf_counter()
                nfiles, files_size, files_size_on_disk = calc_dir_size(name)
                print(f"\r[{nfiles}]")
                print_file_size(files_size)
                print()
                # term_color('4')
                # delta = files_size_on_disk - files_size
                # print((' + ' if delta >= 0 else ' - ') + f'{abs(delta):_}'.replace('_', "'"))
            print_times(os.stat(name))

        else:
            if not os.path.exists(name):
                show(cmd + ' ' + red(oname))

            elif os.path.isfile(name): # `? filename`
                stat = os.stat(name)
                term_color('6')
                print_file_size(stat.st_size)
                term_color('4')
                delta = file_size_on_disk(stat, name) - stat.st_size
                print((' + ' if delta >= 0 else ' - ') + f'{abs(delta):_}'.replace('_', "'"))
                print_times(stat)

            else: # `? .` / `? subdir`
                assert(os.path.isdir(name))
                cols = os.get_terminal_size().columns
                INTERVAL = 2

                names = []
                for de in os.scandir(name):
                    if de.name[0] == '.' and de.is_dir(): # skip hidden directories
                        continue
                    names.append(de.name + '/' * de.is_dir())
                names.sort(key = lambda s: s.casefold())

                rows = 1
                while rows < len(names):
                    total_width = 0
                    width = 0
                    for i in range(len(names)):
                        width = max(width, len(names[i]))
                        if (i + 1) % rows == 0:
                            total_width += width + INTERVAL
                            if total_width > cols:
                                break
                            width = 0
                    else:
                        if total_width + width < cols:
                            break
                    rows += 1

                cols_width = []
                width = 0
                for i in range(len(names)):
                    width = max(width, len(names[i]))
                    if (i + 1) % rows == 0:
                        cols_width.append(width + INTERVAL)
                        width = 0
                if width != 0:
                    cols_width.append(0) # append `0` because the width of the last column makes no sense
                elif len(cols_width) > 0:
                    cols_width[-1] = 0 # mostly for cases when rows == len(names) and the maximum length of names exceeds the width of the terminal

                term_color('6')
                for row in range(rows):
                    for col, col_width in enumerate(cols_width):
                        i = col * rows + row
                        if i < len(names):
                            print(names[i].ljust(col_width).replace('/', term_colors['4'] + '/' + term_colors['6']), end = '')
                    print()

    else:
        show(red('?'))

    print()

finally:
    if os.getcwd() != orig_cwd: # move `.-` to its original location
        res, command = do_command('. = ' + os.path.relpath(orig_cwd, os.getcwd()).replace('\\', '/'))
        assert(res == COMMAND_OK)
        action(command)
